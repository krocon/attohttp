use mimetype::mimetype::mimetype_by_filename;
use std::env;
use std::fs::File;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::thread;

mod mimetype;


///  My first rust code:
///  This is just a small demonstration, a pure learning project.
///  Don't use it in production!
///
///  For production you can use "nickel.rs".
///      See: https://github.com/nickel-org/nickel.rs
///
///      let mut server = Nickel::new();
///      server.utilize(StaticFilesHandler::new("webroot/"));
///      server.listen("127.0.0.1:6767").unwrap();
///


fn get_url(header: &str) -> &str {
    let v: Vec<&str> = header.split(' ').collect();
    v[1]
}

fn get_file_name_by_url(url: &str) -> String {
    let default = String::from("index.html");
    let mut ret: String = url.to_string();
    ret = ret.replacen("\\", "/", 99);

    if ret.contains('#') {
        let idx = ret.find('#').unwrap();
        ret = substring(ret, 0, idx);

    } else if ret.contains('?') {
        let idx = ret.find('?').unwrap();
        ret = substring(ret, 0, idx);
    }

    if ret == "/" || ret == "" {
        return default;
    }
    if ret.ends_with("/") {
        let mut s = String::from(url);
        s.push_str(&default);
        ret = s;

    } else if !ret.contains('.') {
        let mut s = String::from(url);
        s.push_str("/index.html");
        ret = s;
    }

    let len = ret.len();
    ret = substring(ret, 1, len - 1);
    ret
}

fn substring(s: String, start: usize, len: usize) -> String {
    s.chars().skip(start).take(len).collect()
}


fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 512];
    stream.read(&mut buffer).unwrap();

    let s = String::from_utf8_lossy(&buffer);
    let dir: &str = get_url(&s);
    let file_name = get_file_name_by_url(dir);
    let mime_type = mimetype_by_filename(&file_name);

    match File::open(&file_name) {
        Ok(f) => {
            let headers = [
                "HTTP/1.1 200 OK",
                &["Content-type: ", &mime_type].concat(),
                "\r\n"
            ];
            let mut buf = Vec::new();
            let mut file = f;
            match file.read_to_end(&mut buf) {
                Ok(_t) => {
                    let response = {
                        let mut res = headers
                            .join("\r\n")
                            .to_string()
                            .into_bytes();

                        res.extend(&buf);
                        res
                    };
                    stream.write(&response).unwrap();
                    stream.flush().unwrap();
                }
                Err(_) => {}
            }
        }
        Err(_) => {
            println!("Warning. Could not find {}", &file_name);
        } //panic!("Error: Could not find {}.", file_name),
    };
}


fn main() {
    const NAME: &'static str = env!("CARGO_PKG_NAME");
    const VERSION: &'static str = env!("CARGO_PKG_VERSION");
    const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
    const DESCRIPTION: &'static str = env!("CARGO_PKG_DESCRIPTION");

    let args: Vec<String> = env::args().collect();
    let args_len = args.len();
    let port: &str = match args_len {
        2 => &args[1],
        _ => "8080",
    };

    let mut address: String = "127.0.0.1:".to_owned();
    address.push_str(port);

    println!("\n{} V.{}", NAME, VERSION);
    println!("Written by {}", AUTHORS);
    println!("{}", DESCRIPTION);
    println!("\nListening for connections @ {}\n", address);

    let listener = TcpListener::bind(address.to_string()).unwrap();
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                thread::spawn(|| {
                    handle_connection(stream)
                });
            }
            Err(e) => eprintln!("Unable to connect: {}", e)
        }
    }
}

#[cfg(test)]
mod test;
