

#[test]
fn get_file_name_by_url_test_normal() {
    assert_eq!(super::get_file_name_by_url("/index.html"), "index.html");
}

#[test]
fn get_file_name_by_url_test_dir_only_1() {
    assert_eq!(super::get_file_name_by_url("/test"), "test/index.html");
}

#[test]
fn get_file_name_by_url_test_dir_only_2() {
    assert_eq!(super::get_file_name_by_url("/test/"), "test/index.html");
}

#[test]
fn get_file_name_by_url_test_missing_index_html() {
    assert_eq!(super::get_file_name_by_url("/"), "index.html");
}

#[test]
fn get_file_name_by_url_test_parent_dir() {
    assert_eq!(super::get_file_name_by_url("/test/../test.html"), "test/../test.html");
}

#[test]
fn get_file_name_by_url_test_backslash() {
    assert_eq!(super::get_file_name_by_url("/test\\hallo.html"), "test/hallo.html");
}

#[test]
fn get_file_name_by_url_test_dash_in_url() {
    assert_eq!(super::get_file_name_by_url("/#/dashboard"), "index.html");
}

#[test]
fn get_file_name_by_url_test_questionmark_in_url() {
    assert_eq!(super::get_file_name_by_url("/assets/font-awesome-4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0"), "assets/font-awesome-4.7.0/fonts/fontawesome-webfont.woff2");
}
